import React from "react";
import {render} from "react-native-testing-library";

import {Login} from "../../src/pages";

describe('Login page', () =>{
  it("should contains email/password inputs ", () => {
    const { getByPlaceholder, getByTestId} = render(<Login/>)

    expect(getByPlaceholder('Email')).toBeTruthy();
    expect(getByPlaceholder('Password')).toBeTruthy();

    expect(getByTestId('email-id')).toBeTruthy();
    expect(getByTestId('password-id')).toBeTruthy();
  });
});
