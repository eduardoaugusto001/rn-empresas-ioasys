import PropTypes from 'prop-types';
const FontPropType = PropTypes.string;
const ColorPropType = PropTypes.string;

export const ThemePropTypes = PropTypes.shape({
  colors: PropTypes.shape({
    background: ColorPropType,
    dark: ColorPropType,
    dark_gray: ColorPropType,
    gray: ColorPropType,
    light_gray: ColorPropType,
    primary: ColorPropType,
    secondary: ColorPropType,
    success: ColorPropType,
    error: ColorPropType,
    warning: ColorPropType,
    black: ColorPropType,
  }),
  fontSize: PropTypes.shape({
    display: FontPropType,
    heading: FontPropType,
    paragraph: FontPropType,
    content: FontPropType,
    body: FontPropType,
    button: FontPropType,
    header: FontPropType,
    helper: FontPropType,
  }),
});
