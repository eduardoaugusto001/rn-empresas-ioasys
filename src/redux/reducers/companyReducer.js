import {
  COMPANY_CLEAR,
  COMPANIES_FETCH,
  COMPANY_FETCH,
} from '../../constants/actionsType';
import _ from 'lodash';

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action = null) => {
  switch (action.type) {
    case COMPANIES_FETCH:
      return _.mapKeys(action.payload, 'id');
    case COMPANY_CLEAR:
      return {};
    default:
      return state;
  }
};
