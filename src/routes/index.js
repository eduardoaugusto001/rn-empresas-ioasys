import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import AppRoutes from './app.routes';
import AuthRoutes from './auth.routes';

const MainStack = createStackNavigator();

const Routes = () => {
  return (
    <MainStack.Navigator headerMode="none" screenOptions={{headerShown: false}}>
      <MainStack.Screen name="Authentication" component={AuthRoutes} />
      <MainStack.Screen name="Main" component={AppRoutes} />
    </MainStack.Navigator>
  );
};

export default Routes;
