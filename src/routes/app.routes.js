import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Home, Details} from '../pages';
import {BackButton} from '../components';

const App = createStackNavigator();

const AppRoutes = () => (
  <App.Navigator
    screenOptions={{
      headerTransparent: true,
      headerTintColor: 'transparent',
      headerBackTitleVisible: false,
    }}>
    <App.Screen name="Home" component={Home} options={{headerTitle: null}} />
    <App.Screen
      name="Details"
      component={Details}
      options={({navigation}) => ({
        headerBackImage: (props) => (
          <BackButton onPress={() => navigation.goBack()} />
        ),
      })}
    />
  </App.Navigator>
);

export default AppRoutes;
