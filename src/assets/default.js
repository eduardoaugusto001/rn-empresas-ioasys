import * as icons from './icons';

const defaultAssets = {
  icons,
};

export default defaultAssets;
