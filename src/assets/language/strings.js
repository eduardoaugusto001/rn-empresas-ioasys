export const AppHome = {
  welcome: 'Hello',
  search: 'Search company',
};

export const DetailsStrings = {
  sharePrice: 'Share Price:',
  aboutCompany: 'About the Company',
  moreDetails: 'More Details',
  emailEnterprise: 'Email: ',
  facebook: 'Facebook: ',
  twitter: 'Twitter: ',
  linkedin: 'Linkedin: ',
  phone: 'Phone: ',
  address: 'Address: ',
};
