import React from 'react';
import {
  Wrapper,
  Row,
  Thumbnail,
  Column,
  CompanyName,
  CompanyType,
} from './style';

const ListItem = ({companies, onClick}) => {
  return <ListItemContent company={companies} onPress={onClick} />;
};

export default ListItem;

function ListItemContent({company, onPress}) {
  const {enterprise_name, photo, enterprise_type} = company;
  const {enterprise_type_name} = enterprise_type;

  return (
    <Wrapper onPress={onPress}>
      <Row>
        <Thumbnail source={{uri: 'https://empresas.ioasys.com.br/' + photo}} />
        <Column>
          <CompanyName>{enterprise_name}</CompanyName>
          <CompanyType>{enterprise_type_name}</CompanyType>
        </Column>
      </Row>
    </Wrapper>
  );
}
