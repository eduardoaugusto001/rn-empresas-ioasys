import React from 'react';
import {Container, TextInput} from './styles';
import {useAssets} from '../../../assets';
import {useLanguage} from '../../../assets/language';

const Search = ({...rest}) => {
  const {AppHome: string} = useLanguage();
  const {SearchIcon} = useAssets().icons;
  return (
    <Container>
      <TextInput
        keyboardAppearance="dark"
        placeholderTextColor="#666360"
        placeholder={`${string.search}`}
        autoCapitalize="none"
        autoCompleteType="off"
        {...rest}
      />
      <SearchIcon />
    </Container>
  );
};

export default Search;
