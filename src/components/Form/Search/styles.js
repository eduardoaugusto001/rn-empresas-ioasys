import styled from 'styled-components/native';
import {DefaultTheme} from '../../../theme';

export const Container = styled.View`
  width: 100%;
  height: 56px;
  flex-direction: row;
  padding: 0 16px;
  margin: 16px 0;
  align-items: center;
  background: ${DefaultTheme.colors.background};
  border-radius: 12px;
  border-width: 1px;
  border-color: ${DefaultTheme.colors.gray};
`;

export const TextInput = styled.TextInput`
  flex: 1;
  color: ${DefaultTheme.colors.dark_gray};
  font-size: 18px;
`;
