import styled from 'styled-components/native';
import {DefaultTheme} from '../../theme/index';

export const Heading = styled.Text`
  font-size: ${DefaultTheme.fontSize.heading};
  color: ${DefaultTheme.colors.dark};
`;

export const Paragraph = styled.Text`
  font-size: ${DefaultTheme.fontSize.paragraph};
  color: ${DefaultTheme.colors.dark_gray};
`;

export const Content = styled.Text`
  font-size: ${DefaultTheme.fontSize.content};
  color: ${DefaultTheme.colors.dark};
`;

export const Button = styled.Text`
  font-size: ${DefaultTheme.fontSize.button};
  color: ${DefaultTheme.colors.background};
`;

export const Display = styled.Text`
  font-size: ${DefaultTheme.fontSize.display};
  color: ${DefaultTheme.colors.black};
`;

export const Header = styled.Text`
  font-size: ${DefaultTheme.fontSize.header};
  color: ${DefaultTheme.colors.black};
`;

export const Body = styled.Text`
  font-size: ${DefaultTheme.fontSize.body};
  color: ${DefaultTheme.colors.dark};
`;

export const Helper = styled.Text`
  font-size: ${DefaultTheme.fontSize.helper};
  color: ${DefaultTheme.colors.error};
`;
