import React from 'react';
import {Container, ButtonText, Loading} from './styles';

const BackButton = ({label, loading, ...rest}) => (
  <Container {...rest}>
    {loading ? <Loading /> : <ButtonText>{label}</ButtonText> }
  </Container>
);

export default BackButton;
