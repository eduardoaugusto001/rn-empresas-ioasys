import styled from 'styled-components/native';
import {RectButton} from 'react-native-gesture-handler';
import {Button} from '../../GenericText';
import {DefaultTheme} from '../../../theme';

export const Container = styled(RectButton)`
  width: 100%;
  height: 56px;
  background: ${DefaultTheme.colors.primary};
  border-radius: 16px;
  margin-top: 20px;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled(Button)`
  text-align: center;
`;

export const Loading = styled.ActivityIndicator.attrs({
  size: 'small',
  color: '#FFF',
})``;
