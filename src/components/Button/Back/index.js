import React from 'react';
import {Container} from './styles';

import {useAssets} from '../../../assets';

const Button = ({...rest}) => {
  const {HeaderBackIcon} = useAssets().icons;

  return (
    <Container {...rest}>
      <HeaderBackIcon />
    </Container>
  );
};

export default Button;
