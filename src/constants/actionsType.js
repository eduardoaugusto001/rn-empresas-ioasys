//Alert
export const ALERT_MESSAGE = 'ALERT_MESSAGE';

//Auth
export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const AUTH_SET_USER = 'AUTH_SET_USER';
export const AUTH_CLEAR = 'AUTH_CLEAR';

//Company
export const COMPANIES_FETCH = 'COMPANIES_FETCH';
export const COMPANY_FETCH = 'COMPANY_FETCH';
export const COMPANY_CLEAR = 'COMPANY_CLEAR';
