import React, {useLayoutEffect} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';

import {useLanguage} from '../../assets/language';

import {
  Container,
  Banner,
  HeaderTitle,
  Price,
  PriceText,
  Title,
  Description,
  Wrapper,
} from './styles';
import {FORMAT_PRICE} from '../../utils/format-price';
import {getValidationStrings} from '../../utils/getValidationStrings';
import illustration from '../../assets/images/banner.png';

const Details = () => {
  const {DetailsStrings: string} = useLanguage();

  const navigation = useNavigation();
  const route = useRoute();

  const {company} = route.params;
  const {
    enterprise_name,
    share_price,
    description,
    email_enterprise,
    facebook,
    twitter,
    linkedin,
    phone,
    city,
    country,
  } = company;

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: () => <HeaderTitle>{enterprise_name}</HeaderTitle>,
    });
  }, [enterprise_name, navigation]);

  return (
    <>
      <Banner source={illustration} />
      <Container>
        <Price>
          <PriceText>{`${string.sharePrice} ${FORMAT_PRICE(
            share_price,
          )}`}</PriceText>
        </Price>
        <Wrapper>
          <Title>{`${string.aboutCompany} `}</Title>
          <Description>{description}</Description>
        </Wrapper>

        <Wrapper>
          <Title>{`${string.moreDetails}`}</Title>
          <Description>{`${string.emailEnterprise} ${getValidationStrings(
            email_enterprise,
          )}`}</Description>
          <Description>{`${string.facebook} ${getValidationStrings(
            facebook,
          )}`}</Description>
          <Description>{`${string.twitter} ${getValidationStrings(
            twitter,
          )}`}</Description>
          <Description>{`${string.linkedin} ${getValidationStrings(
            linkedin,
          )}`}</Description>
          <Description>{`${string.phone} ${getValidationStrings(
            phone,
          )}`}</Description>
          <Description>{`${string.address} ${city} - ${country}`}</Description>
        </Wrapper>
      </Container>
    </>
  );
};
export default Details;
