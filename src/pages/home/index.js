import React, {useState, useEffect} from 'react';
import {
  KeyboardAvoidingView,
  ScrollView,
  Platform,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

import {useLanguage} from '../../assets/language';
import {getCompanies} from '../../redux/actions';

import {Search, CompanyEmpty, CompanyList} from '../../components';
import {Container, Hello, Name} from './styles';
import {DefaultTheme} from '../../theme';

const Home = () => {
  const {AppHome: string} = useLanguage();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const company = useSelector((state) => Object.values(state.company));

  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');
  const [username, setUserName] = useState('');

  const [filteredDataSource, setFilteredDataSource] = useState([]);

  function renderItem({item}) {
    return (
      <CompanyList
        companies={item}
        onClick={() => navigation.navigate('Details', {company: item})}
      />
    );
  }

  function renderEmptyContent() {
    return <CompanyEmpty />;
  }

  useEffect(() => {
    dispatch(
      getCompanies(
        (response) => {
          setLoading(false);
          setFilteredDataSource(response);
        },
        () => {
          setLoading(false);
          console.log('falhou');
        },
      ),
    );

    getUserAsyncStorage();
  }, [dispatch]);

  async function getUserAsyncStorage() {
    try {
      const user_name = await AsyncStorage.getItem('user_name');
      setUserName(user_name);
    } catch (e) {
      console.log('Error load user name:', e.error);
    }
  }

  const searchFilter = (text) => {
    if (text) {
      const newData = company.filter((item) => {
        const {enterprise_name, enterprise_type} = item;
        const {enterprise_type_name} = enterprise_type;

        const itemData = `${enterprise_name.toUpperCase()} ${enterprise_type_name.toUpperCase()}`;
        const textData = text.toUpperCase();

        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(company);
      setSearch(text);
    }
    setSearch(text);
  };

  return (
    <>
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        enabled>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{flex: 1}}>
          <Container>
            <Hello>{`${string.welcome}`}</Hello>
            <Name>{username}</Name>
            <Search
              value={search}
              onChangeText={(text) => searchFilter(text)}
            />
            <FlatList
              data={filteredDataSource}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
              showsVerticalScrollIndicator={false}
              ListEmptyComponent={renderEmptyContent}
              ListHeaderComponent={() =>
                loading ? (
                  <ActivityIndicator
                    size="small"
                    color={DefaultTheme.colors.primary}
                  />
                ) : null
              }
            />
          </Container>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};

export default Home;
