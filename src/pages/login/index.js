import React, {useCallback, useRef, useState} from 'react';
import {useDispatch} from 'react-redux';

import {KeyboardAvoidingView, ScrollView, Platform} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {showMessage} from 'react-native-flash-message';
import {Form} from '@unform/mobile';
import * as Yup from 'yup';

import {authLogin} from '../../redux/actions';
import {Button, Input} from '../../components';
import {Container, Illustration, Title, Subtitle, BottonTitle} from './styles';

import illustration from '../../assets/images/illustration.png';
import getValidationErrors from '../../utils/getValidationErrors';

const Login = () => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const formRef = useRef(null);
  const passwordInputRef = useRef(null);

  const navigation = useNavigation();

  const handleSignIn = useCallback(
    async (data) => {
      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          email: Yup.string()
            .required('E-mail obrigatório')
            .email('Digite um e-mail válido'),
          password: Yup.string().required('Senha obrigatória'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        dispatch(
          authLogin(
            data,
            () => {
              setLoading(false);
              navigation.navigate('Main');
            },
            () => console.warn('falhou'),
          ),
        );
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          console.log(errors);

          formRef.current?.setErrors(errors);

          return;
        }

        showMessage({
          message: 'Ops! Aconteceu algo inesperado, tente novamente.',
          type: 'danger',
        });
      }
    },
    [dispatch, navigation],
  );

  const initialData = {
    email: 'testeapple@ioasys.com.br',
    password: '12341234',
  };

  return (
    <>
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        enabled>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{flex: 1}}>
          <Container>
            <Illustration source={illustration} />
            <Title>Log in</Title>
            <Subtitle>Welcome to app test company ioasys</Subtitle>

            <Form
              ref={formRef}
              onSubmit={handleSignIn}
              initialData={initialData}>
              <Input
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="email-address"
                name="email"
                testId="email-id"
                placeholder="E-mail"
                returnKeyType="next"
                onSubmitEditing={() => {
                  passwordInputRef.current?.focus();
                }}
              />

              <Input
                ref={passwordInputRef}
                name="password"
                testId="password-id"
                placeholder="Senha"
                secureTextEntry
                returnKeyType="send"
                onSubmitEditing={() => {
                  formRef.current?.submitForm();
                }}
              />
              <Button
                label="Login"
                loading={loading}
                onPress={() => {
                  formRef.current?.submitForm();
                }}
              />
            </Form>
            <BottonTitle>By: Eduardo Augusto</BottonTitle>
          </Container>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};

export default Login;
