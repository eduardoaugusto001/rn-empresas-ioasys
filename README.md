# Desafio React Native - ioasys.

Este documento `README.md` tem como objetivo fornecer as informações para executar o projeto teste empresa.

---

### Clone e instale as dependências
* Abra o terminal e digite o comando git clone https://EduardoPetreca@bitbucket.org/EduardoPetreca/empresas.git
* no terminal digite o comando cd empresas.
* após isso execute npm install ou yarn install.

### Executar o projeto
* Executar no ios: execute o comando npx react-native run-ios.
* Executar no android: execute o comando npx react-native run-android.

### Bibliotecas utlizadas
* async-storage: salvar dados locais no aparelho
* react-navigation: realizar a navegação
* unform: criar formulários
* axios: realizar chamada da api
* intl: tratamento de valores retornados da api
* lodash: utilizado para realizar funções utilitárias
* react-native-flash-message: exibir mensagens alertas
* react-native-iphone-x-helper:
* react-native-svg: utilizado para imagens svg
* react-native-testing-library: realizar testes jest
* react-redux: controle de estado
* redux: controle de estado
* redux-thunk: controle de estado
* styled-components: estilização
* yup: válidar formulário

### Projeto executando:

![N|Solid](example_app.gif)
