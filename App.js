import 'react-native-gesture-handler';
import React from 'react';
import {View, StatusBar, YellowBox} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import FlashMessage from 'react-native-flash-message';

import Router from './src/routes';
import store from './src/redux/store';

YellowBox.ignoreWarnings(['']);

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <StatusBar barStyle="dark-content" />
          <Router />
          <FlashMessage position="top" />
        </View>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
